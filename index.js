import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ContactsList from './comps/ContactsList.jsx';

let contacts = [{
	id: 1,
	name: 'Scott',
	phone: '555 333 7777'
},
{
	id: 2,
	name: 'Turner',
	phone: '222 333 4444'
},
{
	id: 3,
	name: 'Jan',
	phone: '777 111 5555'
},
{
	id: 4,
	name: 'Herb',
	phone: '888 222 4444'
}]

class App extends Component {
	render() {
		return (
				<div>
					<h1>Krime Pays</h1>
					<ContactsList contacts={this.props.contacts} />
				</div>			
			)
	}
}

ReactDOM.render(<App contacts={contacts} />, document.getElementById('app'));

