import React, { component } from 'react';

const Contact = ({contact}) => 
	<li>
		{contact.name} {contact.phone}
	</li>	
			
export default Contact;