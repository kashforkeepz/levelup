module.exports ={
  entry:'./index.js',
  output: {
   // path: __dirname,
    filename: 'app.js'
  },
  watch:true,
  module: {
    loaders: [{ 
      test: /\.jsx?$/, 
      loader: 'babel', 
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react']
      }
    }]
  }
}